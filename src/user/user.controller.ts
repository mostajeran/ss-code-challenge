import { Body, Controller, Post } from '@nestjs/common'
import { ApiOperation } from '@nestjs/swagger'
import { CreateUserDto } from './dto/create-user.dto'
import { UserService } from './user.service'

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @ApiOperation({ description: 'register User by name and phone number', summary: 'register user' })
  @Post()
  async createUser(@Body() body: CreateUserDto) {
    const user = await this.userService.createUser(body)
    this.userService.publishOnMQ(user)
    return user
  }
}
