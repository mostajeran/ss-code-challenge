import { ApiProperty } from '@nestjs/swagger'
import { IsString, Matches, MaxLength } from 'class-validator'

export class CreateUserDto {
  @ApiProperty({ required: true })
  @IsString()
  firstName: string

  @ApiProperty({ required: true })
  @IsString()
  lastName: string

  @ApiProperty({ required: true })
  @Matches(/09\d{9}/)
  @MaxLength(11)
  @IsString()
  phoneNumber: string
}
