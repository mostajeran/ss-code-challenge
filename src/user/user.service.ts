import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { CreateUserDto } from './dto/create-user.dto'
import { User, UserDocument } from './schemas/user.schema'
import * as NRP from 'node-redis-pubsub'

@Injectable()
export class UserService {
  nrp: NRP
  constructor(@InjectModel(User.name) private readonly userModel: Model<UserDocument>) {
    this.nrp = new NRP({
      port: 6379,
      scope: 'demo',
    })
  }

  /**
   * save user to database
   *
   * @param createUserDto
   * @returns
   */
  async createUser(createUserDto: CreateUserDto): Promise<UserDocument> {
    return await this.userModel.create(createUserDto)
  }

  /**
   * publish on redis PUB/SUB
   *
   * @param data
   */
  publishOnMQ(data: UserDocument) {
    this.nrp.emit('user-notify', data)
  }
}
